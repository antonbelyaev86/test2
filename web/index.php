<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Weather\Saver\JsonSaver;
use Weather\Saver\XmlSaver;
use Weather\Formatter\JsonFormatter;
use Weather\Formatter\XmlFormatter;

use AlexTartan\GuzzlePsr18Adapter\Client;
use OpenWeatherMapApi\City;
use OpenWeatherMapApi\OpenWeatherMap;
use OpenWeatherMapApi\Url;

try {
    $city   = new City('Saint Petersburg, RU', 498817);
    $url    = new Url('13a78a2a1f7d95589bdf4460afe64d66', Url::TYPE_FORECAST5, $city);
    $client = new Client();
    $owm    = new OpenWeatherMap($client, $url);

    (new JsonSaver(new JsonFormatter()))->save($owm->getStack());
    (new XmlSaver(new XmlFormatter()))->save($owm->getStack());
} catch (Exception $e) {
    echo $e->getMessage();
    exit;
}

echo "all done";