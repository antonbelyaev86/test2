<?php

namespace Weather\Saver;

use Weather\Formatter\IFormatter;

class JsonSaver extends Saver
{
    /**
     * @var string
     */
    protected $path = self::SAVE_FOLDER.'json.txt';

    /**
     * Сохраняет данные в Json формате
     *
     * @param array $data
     */
    public function save(array $data): void
    {
        $dataToSave = [];

        foreach ($data as $key => $row) {
            $dataToSave[$key] = $this->formatter->format($row, $key);
        }

        $this->check();

        file_put_contents($this->path, json_encode($dataToSave));
    }
}