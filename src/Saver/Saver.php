<?php

namespace Weather\Saver;

use Weather\Formatter\IFormatter;

Abstract class Saver
{
    /** Путь для хранения данных */
    const SAVE_FOLDER = '../data/';

    /**
     * @var IFormatter
     */
    protected $formatter;

    /**
     * @var string
     */
    protected $path;

    /**
     * JsonSaver constructor.
     * @param IFormatter $formatter
     */
    public function __construct(IFormatter $formatter)
    {
        $this->formatter = $formatter;
    }

    /**
     * @param array $data
     */
    abstract function save(array $data): void;

    /**
     * Проверка наличия папки и удаление старых файлов
     */
    protected function check(): void
    {
        if (!file_exists(self::SAVE_FOLDER)) {
            mkdir(self::SAVE_FOLDER, 0777, true);
        }

        if (file_exists($this->path)) {
            unlink($this->path);
        }
    }
}