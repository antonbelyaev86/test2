<?php

namespace Weather\Saver;

use Weather\Formatter\IFormatter;

class XmlSaver extends Saver
{
    /**
     * @var string
     */
    protected $path = self::SAVE_FOLDER.'xml.txt';

    /**
     * Сохраняет данные в Xml формате
     *
     * @param array $data
     */
    public function save(array $data): void
    {
        $dataToSave = [];

        foreach ($data as $key => $row) {
            $dataToSave[$key] = array_flip($this->formatter->format($row, $key));
        }

        $this->check();

        $xml = new \SimpleXMLElement('<root/>');
        array_walk_recursive($dataToSave, array ($xml, 'addChild'));
        file_put_contents($this->path, $xml->asXML());
    }
}