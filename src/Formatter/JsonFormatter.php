<?php

namespace Weather\Formatter;

use OpenWeatherMapApi\Data\Data;

class JsonFormatter implements IFormatter
{
    /**
     * Формирует данные для Json формата
     *
     * @param Data $data
     * @param int $key
     * @return array
     * @throws \Exception
     */
    public function format(Data $data, int $key): array
    {
        $result['date'] = (new \DateTime())->modify("+{$key} day")->format("Y-m-d");
        $result['temp'] = (string)$data->getMain()->getTemp();
        $result['windDeg'] = $data->getWind()->getDeg();

        return $result;
    }
}