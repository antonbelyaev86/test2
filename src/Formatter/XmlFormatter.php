<?php

namespace Weather\Formatter;

use OpenWeatherMapApi\Data\Data;

class XmlFormatter implements IFormatter
{
    /**
     * Формирует данные для Xml формата
     *
     * @param Data $data
     * @param int $key
     * @return array
     * @throws \Exception
     */
    public function format(Data $data, int $key): array
    {
        $result['date'] = (new \DateTime())->modify("+{$key} day")->format("Y-m-d");
        $result['windSpeed'] = $data->getWind()->getSpeed();
        $result['temp'] = (string)$data->getMain()->getTemp();

        return $result;
    }
}