<?php

namespace Weather\Formatter;

use OpenWeatherMapApi\Data\Data;

interface IFormatter
{
    /**
     * @param Data $data
     * @param int $key
     * @return array
     */
    public function format(Data $data, int $key): array;
}